# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ResCompanyUsersRel'
        db.create_table(u'res_company_users_rel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cid', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCompany'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResUsers'])),
        ))
        db.send_create_signal(u'backend', ['ResCompanyUsersRel'])

        # Adding model 'ProductSupplierinfo'
        db.create_table(u'product_supplierinfo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'])),
            ('sequence', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCompany'], null=True, blank=True)),
            ('qty', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('delay', self.gf('django.db.models.fields.IntegerField')()),
            ('min_qty', self.gf('django.db.models.fields.FloatField')()),
            ('product_code', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('product_name', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductTemplate'])),
        ))
        db.send_create_signal(u'backend', ['ProductSupplierinfo'])

        # Adding model 'ProductProduct'
        db.create_table(u'product_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('ean13', self.gf('django.db.models.fields.CharField')(max_length=13, blank=True)),
            ('color', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('price_extra', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('default_code', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('name_template', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('variants', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('product_strength', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductStrength'], null=True, blank=True)),
            ('product_tmpl', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductTemplate'])),
            ('product_image', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('price_margin', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ProductProduct'])

        # Adding model 'SmkLqdOrderBatch'
        db.create_table(u'smk_lqd_order_batch', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('envelope_printed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('manual', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('created_on', self.gf('django.db.models.fields.DateField')()),
            ('printed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('batch_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('require_envelope', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('potential_fraud', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'backend', ['SmkLqdOrderBatch'])

        # Adding model 'ResPartnerCategoryRel'
        db.create_table(u'res_partner_category_rel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartnerCategory'])),
            ('partner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'])),
        ))
        db.send_create_signal(u'backend', ['ResPartnerCategoryRel'])

        # Adding model 'ProductStrength'
        db.create_table(u'product_strength', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('display_name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'backend', ['ProductStrength'])

        # Adding model 'ResRequestLink'
        db.create_table(u'res_request_link', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('priority', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('object', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'backend', ['ResRequestLink'])

        # Adding model 'ProductUomCateg'
        db.create_table(u'product_uom_categ', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'backend', ['ProductUomCateg'])

        # Adding model 'ProductUl'
        db.create_table(u'product_ul', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'backend', ['ProductUl'])

        # Adding model 'ProductPriceList'
        db.create_table(u'product_price_list', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('qty1', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('qty2', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('qty3', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('qty4', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('qty5', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('price_list', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'product_price_list', to=orm['backend.ProductPriceList'])),
        ))
        db.send_create_signal(u'backend', ['ProductPriceList'])

        # Adding model 'ResConfig'
        db.create_table(u'res_config', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResConfig'])

        # Adding model 'ProductPackaging'
        db.create_table(u'product_packaging', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('ul', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductUl'])),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=14, blank=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductProduct'])),
            ('weight', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('sequence', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('ul_qty', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('ean', self.gf('django.db.models.fields.CharField')(max_length=14, blank=True)),
            ('qty', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('width', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('length', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('rows', self.gf('django.db.models.fields.IntegerField')()),
            ('height', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('weight_ul', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'backend', ['ProductPackaging'])

        # Adding model 'ResDepartment'
        db.create_table(u'res_department', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=60)),
        ))
        db.send_create_signal(u'backend', ['ResDepartment'])

        # Adding model 'ResWidget'
        db.create_table(u'res_widget', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'backend', ['ResWidget'])

        # Adding model 'ResPayterm'
        db.create_table(u'res_payterm', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResPayterm'])

        # Adding model 'ResPartnerBank'
        db.create_table(u'res_partner_bank', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('bank_name', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('owner_name', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('sequence', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('partner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'])),
            ('bank', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResBank'], null=True, blank=True)),
            ('bank_bic', self.gf('django.db.models.fields.CharField')(max_length=16, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('zip', self.gf('django.db.models.fields.CharField')(max_length=24, blank=True)),
            ('footer', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCountry'], null=True, blank=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCompany'], null=True, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('state_0', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCountryState'], null=True, db_column=u'state_id', blank=True)),
            ('acc_number', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'backend', ['ResPartnerBank'])

        # Adding model 'ResPartnerTitle'
        db.create_table(u'res_partner_title', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('domain', self.gf('django.db.models.fields.CharField')(max_length=24)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=46)),
            ('shortcut', self.gf('django.db.models.fields.CharField')(max_length=16)),
        ))
        db.send_create_signal(u'backend', ['ResPartnerTitle'])

        # Adding model 'ResCurrencyRate'
        db.create_table(u'res_currency_rate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCurrency'], null=True, blank=True)),
            ('rate', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('name', self.gf('django.db.models.fields.DateField')()),
            ('currency_rate_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCurrencyRateType'], null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResCurrencyRate'])

        # Adding model 'ProductUom'
        db.create_table(u'product_uom', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('uom_type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductUomCateg'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('rounding', self.gf('django.db.models.fields.DecimalField')(max_digits=65535, decimal_places=65535)),
            ('factor', self.gf('django.db.models.fields.DecimalField')(max_digits=65535, decimal_places=65535)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'backend', ['ProductUom'])

        # Adding model 'SmkLqdMessage'
        db.create_table(u'smk_lqd_message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('sender', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'], null=True, blank=True)),
            ('replier_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResUsers'], null=True, db_column=u'replier_user', blank=True)),
            ('reply_body', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('was_replied', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_public', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('from_site', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('sent_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal(u'backend', ['SmkLqdMessage'])

        # Adding model 'ProductTemplate'
        db.create_table(u'product_template', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('warranty', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('supply_method', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('uos', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'product_template_uos', null=True, to=orm['backend.ProductUom'])),
            ('list_price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('weight', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('standard_price', self.gf('django.db.models.fields.DecimalField')(max_digits=65535, decimal_places=65535)),
            ('mes_type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('uom', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'product_template_uom', to=orm['backend.ProductUom'])),
            ('description_purchase', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('uos_coeff', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('purchase_ok', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('product_manager', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResUsers'], null=True, blank=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCompany'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('loc_rack', self.gf('django.db.models.fields.CharField')(max_length=16, blank=True)),
            ('uom_po', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'product_tamplete_uom_po', to=orm['backend.ProductUom'])),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('weight_net', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('volume', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('loc_row', self.gf('django.db.models.fields.CharField')(max_length=16, blank=True)),
            ('description_sale', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('procure_method', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('cost_method', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('rental', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('sale_ok', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('sale_delay', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('loc_case', self.gf('django.db.models.fields.CharField')(max_length=16, blank=True)),
            ('produce_delay', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('categ', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductCategory'])),
        ))
        db.send_create_signal(u'backend', ['ProductTemplate'])

        # Adding model 'ResPartnerAddress'
        db.create_table(u'res_partner_address', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('function', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('color', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('street2', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('partner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'], null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('hbn', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('zip', self.gf('django.db.models.fields.CharField')(max_length=24, blank=True)),
            ('title', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartnerTitle'], null=True, blank=True)),
            ('mobile', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCountry'], null=True, blank=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCompany'], null=True, blank=True)),
            ('birthdate', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('second_name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCountryState'], null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=240, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResPartnerAddress'])

        # Adding model 'ResWidgetUser'
        db.create_table(u'res_widget_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResUsers'], null=True, blank=True)),
            ('widget', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResWidget'])),
            ('sequence', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResWidgetUser'])

        # Adding model 'SmkLqdOrderItem'
        db.create_table(u'smk_lqd_order_item', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductProduct'])),
            ('line_total', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('sale_price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('order_ids', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.SmkLqdOrderOrder'], null=True, db_column=u'order_ids', blank=True)),
            ('product_description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('product_name', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal(u'backend', ['SmkLqdOrderItem'])

        # Adding model 'SmkLqdInvites'
        db.create_table(u'smk_lqd_invites', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('convert_signup', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('sender', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'])),
            ('was_followed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('sent_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('channel', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('followed_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['SmkLqdInvites'])

        # Adding model 'SmkLqdHtmlTemplates'
        db.create_table(u'smk_lqd_html_templates', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('template_content', self.gf('django.db.models.fields.TextField')()),
            ('created_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal(u'backend', ['SmkLqdHtmlTemplates'])

        # Adding model 'ResUsers'
        db.create_table(u'res_users', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('login', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('context_tz', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('signature', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('context_lang', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('company_id', self.gf('django.db.models.fields.IntegerField')()),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('menu_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('menu_tips', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('department_ids', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResDepartment'], null=True, blank=True)),
            ('action_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('user_email', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResUsers'])

        # Adding model 'ResPartnerCategory'
        db.create_table(u'res_partner_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent_left', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('parent_right', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartnerCategory'], null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'backend', ['ResPartnerCategory'])

        # Adding model 'ProductInstaller'
        db.create_table(u'product_installer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('customers', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'backend', ['ProductInstaller'])

        # Adding model 'ResCompany'
        db.create_table(u'res_company', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_uid', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('rml_header', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'company_rml_header', db_column=u'rml_header', to=orm['backend.ResUsers'])),
            ('rml_footer1', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('paper_format', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('currency_id', self.gf('django.db.models.fields.IntegerField')()),
            ('rml_header2', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCurrency'], db_column=u'rml_header2')),
            ('rml_header3', self.gf('django.db.models.fields.TextField')()),
            ('rml_header1', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('logo', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('partner_id', self.gf('django.db.models.fields.IntegerField')()),
            ('account_no', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'], null=True, db_column=u'account_no', blank=True)),
            ('company_registry', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResCompany'])

        # Adding model 'ResOwner'
        db.create_table(u'res_owner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('comment', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResOwner'])

        # Adding model 'ResWidgetWizard'
        db.create_table(u'res_widget_wizard', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('widgets_list', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'backend', ['ResWidgetWizard'])

        # Adding model 'ProductCategory'
        db.create_table(u'product_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent_left', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('parent_right', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('create_uid', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'create_user', null=True, to=orm['backend.ResUsers'])),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_uid', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'update_user', null=True, to=orm['backend.ResUsers'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('sequence', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductCategory'], null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ProductCategory'])

        # Adding model 'ProductPricelistVersion'
        db.create_table(u'product_pricelist_version', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('pricelist', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductPriceList'])),
            ('date_end', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('date_start', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('company_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ProductPricelistVersion'])

        # Adding model 'ResPartnerBankType'
        db.create_table(u'res_partner_bank_type', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('format_layout', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResPartnerBankType'])

        # Adding model 'ResCurrencyRateType'
        db.create_table(u'res_currency_rate_type', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'backend', ['ResCurrencyRateType'])

        # Adding model 'ResRequest'
        db.create_table(u'res_request', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('body', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('date_sent', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('ref_doc2', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('priority', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ref_doc1', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('act_from', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'request_act_from', db_column=u'act_from', to=orm['backend.ResUsers'])),
            ('ref_partner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'], null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('trigger_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('act_to', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'request_act_to', db_column=u'act_to', to=orm['backend.ResUsers'])),
        ))
        db.send_create_signal(u'backend', ['ResRequest'])

        # Adding model 'ResCountryState'
        db.create_table(u'res_country_state', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCountry'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'backend', ['ResCountryState'])

        # Adding model 'ResCountry'
        db.create_table(u'res_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('address_format', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=2)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
        ))
        db.send_create_signal(u'backend', ['ResCountry'])

        # Adding model 'ResRequestHistory'
        db.create_table(u'res_request_history', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('body', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('act_from', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'request_history_act_from', db_column=u'act_from', to=orm['backend.ResUsers'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('req', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResRequest'])),
            ('date_sent', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'backend', ['ResRequestHistory'])

        # Adding model 'ResBank'
        db.create_table(u'res_bank', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('zip', self.gf('django.db.models.fields.CharField')(max_length=24, blank=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCountry'], null=True, blank=True)),
            ('street2', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('bic', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCountryState'], null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResBank'])

        # Adding model 'SmkLqdSubsStandingOrder'
        db.create_table(u'smk_lqd_subs_standing_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('cancelled_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('free_vaporizer_count', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('successful_billing_count', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('last_batchid', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('snail_mail_counter', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'])),
            ('last_order_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('request_cancellation', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cohort_index', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('next_delivery', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('last_delivery', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('remote_addr', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.SmkLqdRemoteAddr'], null=True, blank=True)),
            ('cancelled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('order_counter', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('battery_counter', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('potential_fraud', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('flavours', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.SmkLqdSubsFlavours'], null=True, blank=True)),
            ('free_flavours', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.SmkLqdSubsFreeFlavours'], null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['SmkLqdSubsStandingOrder'])

        # Adding model 'ProductPriceType'
        db.create_table(u'product_price_type', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('field', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCurrency'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'backend', ['ProductPriceType'])

        # Adding model 'ResPartnerBankTypeField'
        db.create_table(u'res_partner_bank_type_field', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('bank_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartnerBankType'])),
            ('readonly', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('required', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('size', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResPartnerBankTypeField'])

        # Adding model 'SmkLqdSubsFlavours'
        db.create_table(u'smk_lqd_subs_flavours', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('flavour1', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour1', null=True, to=orm['backend.ProductProduct'])),
            ('flavour10', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour10', null=True, to=orm['backend.ProductProduct'])),
            ('flavour3', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour3', null=True, to=orm['backend.ProductProduct'])),
            ('flavour2', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour2', null=True, to=orm['backend.ProductProduct'])),
            ('flavour5', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour5', null=True, to=orm['backend.ProductProduct'])),
            ('flavour4', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour4', null=True, to=orm['backend.ProductProduct'])),
            ('flavour7', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour7', null=True, to=orm['backend.ProductProduct'])),
            ('flavour6', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour6', null=True, to=orm['backend.ProductProduct'])),
            ('flavour9', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour9', null=True, to=orm['backend.ProductProduct'])),
            ('flavour8', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'flavour_flavour8', null=True, to=orm['backend.ProductProduct'])),
        ))
        db.send_create_signal(u'backend', ['SmkLqdSubsFlavours'])

        # Adding model 'ResCurrency'
        db.create_table(u'res_currency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('rounding', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('symbol', self.gf('django.db.models.fields.CharField')(max_length=3, blank=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCompany'], null=True, blank=True)),
            ('date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('base', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('accuracy', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResCurrency'])

        # Adding model 'ResPartner'
        db.create_table(u'res_partner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('website', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('ean13', self.gf('django.db.models.fields.CharField')(max_length=13, blank=True)),
            ('color', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResOwner'], null=True, blank=True)),
            ('supplier', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResUsers'], null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartnerTitle'], null=True, blank=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResCompany'], null=True, blank=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'], null=True, blank=True)),
            ('password_blank', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('employee', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ref', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('vat', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('lang', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('customer', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('credit_limit', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('comment', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResPartner'])

        # Adding model 'SmkLqdSubsFreeFlavours'
        db.create_table(u'smk_lqd_subs_free_flavours', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_uid', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'free_flavour_create_user', null=True, to=orm['backend.ResUsers'])),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_uid', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'free_flavour_update_user1', null=True, to=orm['backend.ResUsers'])),
            ('flavour1', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'free_flavour_flavour1', null=True, to=orm['backend.ProductProduct'])),
            ('flavour3', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'free_flavour_flavour3', null=True, to=orm['backend.ProductProduct'])),
            ('flavour2', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'free_flavour_flavour2', null=True, to=orm['backend.ProductProduct'])),
            ('flavour4', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'free_flavour_flavour4', null=True, to=orm['backend.ProductProduct'])),
        ))
        db.send_create_signal(u'backend', ['SmkLqdSubsFreeFlavours'])

        # Adding model 'ResConfigInstaller'
        db.create_table(u'res_config_installer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResConfigInstaller'])

        # Adding model 'SmkLqdOrderOrder'
        db.create_table(u'smk_lqd_order_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('customer_delivery_street_line2', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('customer_billing_street', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('customer_email', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('require_envelope', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('potential_fraud', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('envelope_printed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('customer_phone', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('customer_delivery_county', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('customer_billing_county', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('customer_billing_postcode', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('created_on', self.gf('django.db.models.fields.DateField')()),
            ('printed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('customer_billing_country', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('customer_billing_hbn', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('customer_billing_city', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'])),
            ('order_total', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('customer_delivery_postcode', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('customer_billing_street_line2', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('customer_delivery_street', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('manual', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('customer_mobile', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('customer_delivery_hbn', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('batch_ids', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.SmkLqdOrderBatch'], null=True, db_column=u'batch_ids', blank=True)),
            ('customer_delivery_city', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('customer_name', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('customer_fax', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('customer_delivery_country', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('trans_id', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'backend', ['SmkLqdOrderOrder'])

        # Adding model 'ResPartnerEvent'
        db.create_table(u'res_partner_event', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('partner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResPartner'], null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ResUsers'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'backend', ['ResPartnerEvent'])

        # Adding model 'ProductPricelistType'
        db.create_table(u'product_pricelist_type', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'backend', ['ProductPricelistType'])

        # Adding model 'ProductPricelistItem'
        db.create_table(u'product_pricelist_item', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('price_round', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('price_discount', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('base_pricelist', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductPriceList'], null=True, blank=True)),
            ('sequence', self.gf('django.db.models.fields.IntegerField')()),
            ('price_max_margin', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('company_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('product_tmpl', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductTemplate'], null=True, blank=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductProduct'], null=True, blank=True)),
            ('base', self.gf('django.db.models.fields.IntegerField')()),
            ('price_version', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductPricelistVersion'])),
            ('min_quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('price_min_margin', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
            ('categ', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.ProductCategory'], null=True, blank=True)),
            ('price_surcharge', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=65535, decimal_places=65535, blank=True)),
        ))
        db.send_create_signal(u'backend', ['ProductPricelistItem'])

        # Adding model 'SmkLqdRemoteAddr'
        db.create_table(u'smk_lqd_remote_addr', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('create_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('write_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('addr', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'backend', ['SmkLqdRemoteAddr'])


    def backwards(self, orm):
        # Deleting model 'ResCompanyUsersRel'
        db.delete_table(u'res_company_users_rel')

        # Deleting model 'ProductSupplierinfo'
        db.delete_table(u'product_supplierinfo')

        # Deleting model 'ProductProduct'
        db.delete_table(u'product_product')

        # Deleting model 'SmkLqdOrderBatch'
        db.delete_table(u'smk_lqd_order_batch')

        # Deleting model 'ResPartnerCategoryRel'
        db.delete_table(u'res_partner_category_rel')

        # Deleting model 'ProductStrength'
        db.delete_table(u'product_strength')

        # Deleting model 'ResRequestLink'
        db.delete_table(u'res_request_link')

        # Deleting model 'ProductUomCateg'
        db.delete_table(u'product_uom_categ')

        # Deleting model 'ProductUl'
        db.delete_table(u'product_ul')

        # Deleting model 'ProductPriceList'
        db.delete_table(u'product_price_list')

        # Deleting model 'ResConfig'
        db.delete_table(u'res_config')

        # Deleting model 'ProductPackaging'
        db.delete_table(u'product_packaging')

        # Deleting model 'ResDepartment'
        db.delete_table(u'res_department')

        # Deleting model 'ResWidget'
        db.delete_table(u'res_widget')

        # Deleting model 'ResPayterm'
        db.delete_table(u'res_payterm')

        # Deleting model 'ResPartnerBank'
        db.delete_table(u'res_partner_bank')

        # Deleting model 'ResPartnerTitle'
        db.delete_table(u'res_partner_title')

        # Deleting model 'ResCurrencyRate'
        db.delete_table(u'res_currency_rate')

        # Deleting model 'ProductUom'
        db.delete_table(u'product_uom')

        # Deleting model 'SmkLqdMessage'
        db.delete_table(u'smk_lqd_message')

        # Deleting model 'ProductTemplate'
        db.delete_table(u'product_template')

        # Deleting model 'ResPartnerAddress'
        db.delete_table(u'res_partner_address')

        # Deleting model 'ResWidgetUser'
        db.delete_table(u'res_widget_user')

        # Deleting model 'SmkLqdOrderItem'
        db.delete_table(u'smk_lqd_order_item')

        # Deleting model 'SmkLqdInvites'
        db.delete_table(u'smk_lqd_invites')

        # Deleting model 'SmkLqdHtmlTemplates'
        db.delete_table(u'smk_lqd_html_templates')

        # Deleting model 'ResUsers'
        db.delete_table(u'res_users')

        # Deleting model 'ResPartnerCategory'
        db.delete_table(u'res_partner_category')

        # Deleting model 'ProductInstaller'
        db.delete_table(u'product_installer')

        # Deleting model 'ResCompany'
        db.delete_table(u'res_company')

        # Deleting model 'ResOwner'
        db.delete_table(u'res_owner')

        # Deleting model 'ResWidgetWizard'
        db.delete_table(u'res_widget_wizard')

        # Deleting model 'ProductCategory'
        db.delete_table(u'product_category')

        # Deleting model 'ProductPricelistVersion'
        db.delete_table(u'product_pricelist_version')

        # Deleting model 'ResPartnerBankType'
        db.delete_table(u'res_partner_bank_type')

        # Deleting model 'ResCurrencyRateType'
        db.delete_table(u'res_currency_rate_type')

        # Deleting model 'ResRequest'
        db.delete_table(u'res_request')

        # Deleting model 'ResCountryState'
        db.delete_table(u'res_country_state')

        # Deleting model 'ResCountry'
        db.delete_table(u'res_country')

        # Deleting model 'ResRequestHistory'
        db.delete_table(u'res_request_history')

        # Deleting model 'ResBank'
        db.delete_table(u'res_bank')

        # Deleting model 'SmkLqdSubsStandingOrder'
        db.delete_table(u'smk_lqd_subs_standing_order')

        # Deleting model 'ProductPriceType'
        db.delete_table(u'product_price_type')

        # Deleting model 'ResPartnerBankTypeField'
        db.delete_table(u'res_partner_bank_type_field')

        # Deleting model 'SmkLqdSubsFlavours'
        db.delete_table(u'smk_lqd_subs_flavours')

        # Deleting model 'ResCurrency'
        db.delete_table(u'res_currency')

        # Deleting model 'ResPartner'
        db.delete_table(u'res_partner')

        # Deleting model 'SmkLqdSubsFreeFlavours'
        db.delete_table(u'smk_lqd_subs_free_flavours')

        # Deleting model 'ResConfigInstaller'
        db.delete_table(u'res_config_installer')

        # Deleting model 'SmkLqdOrderOrder'
        db.delete_table(u'smk_lqd_order_order')

        # Deleting model 'ResPartnerEvent'
        db.delete_table(u'res_partner_event')

        # Deleting model 'ProductPricelistType'
        db.delete_table(u'product_pricelist_type')

        # Deleting model 'ProductPricelistItem'
        db.delete_table(u'product_pricelist_item')

        # Deleting model 'SmkLqdRemoteAddr'
        db.delete_table(u'smk_lqd_remote_addr')


    models = {
        u'backend.productcategory': {
            'Meta': {'object_name': 'ProductCategory', 'db_table': "u'product_category'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'create_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'create_user'", 'null': 'True', 'to': u"orm['backend.ResUsers']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductCategory']", 'null': 'True', 'blank': 'True'}),
            'parent_left': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'parent_right': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'update_user'", 'null': 'True', 'to': u"orm['backend.ResUsers']"})
        },
        u'backend.productinstaller': {
            'Meta': {'object_name': 'ProductInstaller', 'db_table': "u'product_installer'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'customers': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productpackaging': {
            'Meta': {'object_name': 'ProductPackaging', 'db_table': "u'product_packaging'"},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'ean': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'height': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductProduct']"}),
            'qty': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'rows': ('django.db.models.fields.IntegerField', [], {}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ul': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductUl']"}),
            'ul_qty': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'weight_ul': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productpricelist': {
            'Meta': {'object_name': 'ProductPriceList', 'db_table': "u'product_price_list'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price_list': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'product_price_list'", 'to': u"orm['backend.ProductPriceList']"}),
            'qty1': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'qty2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'qty3': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'qty4': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'qty5': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productpricelistitem': {
            'Meta': {'object_name': 'ProductPricelistItem', 'db_table': "u'product_pricelist_item'"},
            'base': ('django.db.models.fields.IntegerField', [], {}),
            'base_pricelist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductPriceList']", 'null': 'True', 'blank': 'True'}),
            'categ': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductCategory']", 'null': 'True', 'blank': 'True'}),
            'company_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'min_quantity': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'price_discount': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'price_max_margin': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'price_min_margin': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'price_round': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'price_surcharge': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'price_version': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductPricelistVersion']"}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductProduct']", 'null': 'True', 'blank': 'True'}),
            'product_tmpl': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductTemplate']", 'null': 'True', 'blank': 'True'}),
            'sequence': ('django.db.models.fields.IntegerField', [], {}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productpricelisttype': {
            'Meta': {'object_name': 'ProductPricelistType', 'db_table': "u'product_pricelist_type'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productpricelistversion': {
            'Meta': {'object_name': 'ProductPricelistVersion', 'db_table': "u'product_pricelist_version'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'company_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_end': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'pricelist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductPriceList']"}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productpricetype': {
            'Meta': {'object_name': 'ProductPriceType', 'db_table': "u'product_price_type'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCurrency']"}),
            'field': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productproduct': {
            'Meta': {'object_name': 'ProductProduct', 'db_table': "u'product_product'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'default_code': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'ean13': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_template': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'price_extra': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'price_margin': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'product_image': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'product_strength': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductStrength']", 'null': 'True', 'blank': 'True'}),
            'product_tmpl': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductTemplate']"}),
            'variants': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productstrength': {
            'Meta': {'object_name': 'ProductStrength', 'db_table': "u'product_strength'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productsupplierinfo': {
            'Meta': {'object_name': 'ProductSupplierinfo', 'db_table': "u'product_supplierinfo'"},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'delay': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'min_qty': ('django.db.models.fields.FloatField', [], {}),
            'name': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']"}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductTemplate']"}),
            'product_code': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'product_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'qty': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.producttemplate': {
            'Meta': {'object_name': 'ProductTemplate', 'db_table': "u'product_template'"},
            'categ': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductCategory']"}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'blank': 'True'}),
            'cost_method': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_purchase': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_sale': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'loc_case': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'loc_rack': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'loc_row': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'mes_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'procure_method': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'produce_delay': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'product_manager': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'blank': 'True'}),
            'purchase_ok': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'rental': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sale_delay': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'sale_ok': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'standard_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '65535', 'decimal_places': '65535'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'supply_method': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'uom': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'product_template_uom'", 'to': u"orm['backend.ProductUom']"}),
            'uom_po': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'product_tamplete_uom_po'", 'to': u"orm['backend.ProductUom']"}),
            'uos': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'product_template_uos'", 'null': 'True', 'to': u"orm['backend.ProductUom']"}),
            'uos_coeff': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'volume': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'warranty': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'weight_net': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productul': {
            'Meta': {'object_name': 'ProductUl', 'db_table': "u'product_ul'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productuom': {
            'Meta': {'object_name': 'ProductUom', 'db_table': "u'product_uom'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductUomCateg']"}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'factor': ('django.db.models.fields.DecimalField', [], {'max_digits': '65535', 'decimal_places': '65535'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'rounding': ('django.db.models.fields.DecimalField', [], {'max_digits': '65535', 'decimal_places': '65535'}),
            'uom_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productuomcateg': {
            'Meta': {'object_name': 'ProductUomCateg', 'db_table': "u'product_uom_categ'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resbank': {
            'Meta': {'object_name': 'ResBank', 'db_table': "u'res_bank'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'bic': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCountry']", 'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCountryState']", 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'street2': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'})
        },
        u'backend.rescompany': {
            'Meta': {'object_name': 'ResCompany', 'db_table': "u'res_company'"},
            'account_no': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'db_column': "u'account_no'", 'blank': 'True'}),
            'company_registry': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'currency_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'blank': 'True'}),
            'paper_format': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'parent_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'partner_id': ('django.db.models.fields.IntegerField', [], {}),
            'rml_footer1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'rml_header': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'company_rml_header'", 'db_column': "u'rml_header'", 'to': u"orm['backend.ResUsers']"}),
            'rml_header1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'rml_header2': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCurrency']", 'db_column': "u'rml_header2'"}),
            'rml_header3': ('django.db.models.fields.TextField', [], {}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescompanyusersrel': {
            'Meta': {'object_name': 'ResCompanyUsersRel', 'db_table': "u'res_company_users_rel'"},
            'cid': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']"})
        },
        u'backend.resconfig': {
            'Meta': {'object_name': 'ResConfig', 'db_table': "u'res_config'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resconfiginstaller': {
            'Meta': {'object_name': 'ResConfigInstaller', 'db_table': "u'res_config_installer'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescountry': {
            'Meta': {'object_name': 'ResCountry', 'db_table': "u'res_country'"},
            'address_format': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescountrystate': {
            'Meta': {'object_name': 'ResCountryState', 'db_table': "u'res_country_state'"},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCountry']"}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescurrency': {
            'Meta': {'object_name': 'ResCurrency', 'db_table': "u'res_currency'"},
            'accuracy': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'base': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'rounding': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescurrencyrate': {
            'Meta': {'object_name': 'ResCurrencyRate', 'db_table': "u'res_currency_rate'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCurrency']", 'null': 'True', 'blank': 'True'}),
            'currency_rate_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCurrencyRateType']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.DateField', [], {}),
            'rate': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescurrencyratetype': {
            'Meta': {'object_name': 'ResCurrencyRateType', 'db_table': "u'res_currency_rate_type'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resdepartment': {
            'Meta': {'object_name': 'ResDepartment', 'db_table': "u'res_department'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resowner': {
            'Meta': {'object_name': 'ResOwner', 'db_table': "u'res_owner'"},
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartner': {
            'Meta': {'object_name': 'ResPartner', 'db_table': "u'res_partner'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'credit_limit': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'ean13': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'employee': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResOwner']", 'null': 'True', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'blank': 'True'}),
            'password_blank': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'ref': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'supplier': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartnerTitle']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'vat': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartneraddress': {
            'Meta': {'object_name': 'ResPartnerAddress', 'db_table': "u'res_partner_address'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'birthdate': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCountry']", 'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '240', 'blank': 'True'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'function': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'hbn': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'partner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCountryState']", 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'street2': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'title': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartnerTitle']", 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'})
        },
        u'backend.respartnerbank': {
            'Meta': {'object_name': 'ResPartnerBank', 'db_table': "u'res_partner_bank'"},
            'acc_number': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'bank': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResBank']", 'null': 'True', 'blank': 'True'}),
            'bank_bic': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'bank_name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCountry']", 'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'footer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'owner_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'partner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']"}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state_0': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCountryState']", 'null': 'True', 'db_column': "u'state_id'", 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '24', 'blank': 'True'})
        },
        u'backend.respartnerbanktype': {
            'Meta': {'object_name': 'ResPartnerBankType', 'db_table': "u'res_partner_bank_type'"},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'format_layout': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartnerbanktypefield': {
            'Meta': {'object_name': 'ResPartnerBankTypeField', 'db_table': "u'res_partner_bank_type_field'"},
            'bank_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartnerBankType']"}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'readonly': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartnercategory': {
            'Meta': {'object_name': 'ResPartnerCategory', 'db_table': "u'res_partner_category'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartnerCategory']", 'null': 'True', 'blank': 'True'}),
            'parent_left': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'parent_right': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartnercategoryrel': {
            'Meta': {'object_name': 'ResPartnerCategoryRel', 'db_table': "u'res_partner_category_rel'"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartnerCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'partner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']"})
        },
        u'backend.respartnerevent': {
            'Meta': {'object_name': 'ResPartnerEvent', 'db_table': "u'res_partner_event'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'partner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartnertitle': {
            'Meta': {'object_name': 'ResPartnerTitle', 'db_table': "u'res_partner_title'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '46'}),
            'shortcut': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respayterm': {
            'Meta': {'object_name': 'ResPayterm', 'db_table': "u'res_payterm'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resrequest': {
            'Meta': {'object_name': 'ResRequest', 'db_table': "u'res_request'"},
            'act_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'request_act_from'", 'db_column': "u'act_from'", 'to': u"orm['backend.ResUsers']"}),
            'act_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'request_act_to'", 'db_column': "u'act_to'", 'to': u"orm['backend.ResUsers']"}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_sent': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ref_doc1': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'ref_doc2': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'ref_partner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'trigger_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resrequesthistory': {
            'Meta': {'object_name': 'ResRequestHistory', 'db_table': "u'res_request_history'"},
            'act_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'request_history_act_from'", 'db_column': "u'act_from'", 'to': u"orm['backend.ResUsers']"}),
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_sent': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'req': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResRequest']"}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resrequestlink': {
            'Meta': {'object_name': 'ResRequestLink', 'db_table': "u'res_request_link'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'object': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resusers': {
            'Meta': {'object_name': 'ResUsers', 'db_table': "u'res_users'"},
            'action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'company_id': ('django.db.models.fields.IntegerField', [], {}),
            'context_lang': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'context_tz': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'department_ids': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResDepartment']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'menu_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'menu_tips': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'signature': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'user_email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.reswidget': {
            'Meta': {'object_name': 'ResWidget', 'db_table': "u'res_widget'"},
            'content': ('django.db.models.fields.TextField', [], {}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.reswidgetuser': {
            'Meta': {'object_name': 'ResWidgetUser', 'db_table': "u'res_widget_user'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'blank': 'True'}),
            'widget': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResWidget']"}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.reswidgetwizard': {
            'Meta': {'object_name': 'ResWidgetWizard', 'db_table': "u'res_widget_wizard'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'widgets_list': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdhtmltemplates': {
            'Meta': {'object_name': 'SmkLqdHtmlTemplates', 'db_table': "u'smk_lqd_html_templates'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'created_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'template_content': ('django.db.models.fields.TextField', [], {}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdinvites': {
            'Meta': {'object_name': 'SmkLqdInvites', 'db_table': "u'smk_lqd_invites'"},
            'channel': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'convert_signup': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'followed_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sender': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']"}),
            'sent_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'was_followed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdmessage': {
            'Meta': {'object_name': 'SmkLqdMessage', 'db_table': "u'smk_lqd_message'"},
            'body': ('django.db.models.fields.TextField', [], {}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'from_site': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'replier_user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'db_column': "u'replier_user'", 'blank': 'True'}),
            'reply_body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sender': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'blank': 'True'}),
            'sent_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'was_replied': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdorderbatch': {
            'Meta': {'object_name': 'SmkLqdOrderBatch', 'db_table': "u'smk_lqd_order_batch'"},
            'batch_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'created_on': ('django.db.models.fields.DateField', [], {}),
            'envelope_printed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manual': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'potential_fraud': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'printed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'require_envelope': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdorderitem': {
            'Meta': {'object_name': 'SmkLqdOrderItem', 'db_table': "u'smk_lqd_order_item'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'line_total': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'order_ids': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdOrderOrder']", 'null': 'True', 'db_column': "u'order_ids'", 'blank': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductProduct']"}),
            'product_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'product_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'sale_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdorderorder': {
            'Meta': {'object_name': 'SmkLqdOrderOrder', 'db_table': "u'smk_lqd_order_order'"},
            'batch_ids': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdOrderBatch']", 'null': 'True', 'db_column': "u'batch_ids'", 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'created_on': ('django.db.models.fields.DateField', [], {}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']"}),
            'customer_billing_city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer_billing_country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer_billing_county': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer_billing_hbn': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'customer_billing_postcode': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer_billing_street': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'customer_billing_street_line2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'customer_delivery_city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer_delivery_country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer_delivery_county': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer_delivery_hbn': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'customer_delivery_postcode': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'customer_delivery_street': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'customer_delivery_street_line2': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'customer_email': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'customer_fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'customer_mobile': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'customer_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'customer_phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'envelope_printed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manual': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order_total': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'potential_fraud': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'printed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'require_envelope': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'trans_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdremoteaddr': {
            'Meta': {'object_name': 'SmkLqdRemoteAddr', 'db_table': "u'smk_lqd_remote_addr'"},
            'addr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdsubsflavours': {
            'Meta': {'object_name': 'SmkLqdSubsFlavours', 'db_table': "u'smk_lqd_subs_flavours'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'flavour1': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour1'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour10': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour10'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour2'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour3': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour3'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour4': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour4'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour5': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour5'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour6': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour6'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour7': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour7'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour8': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour8'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour9': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour9'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdsubsfreeflavours': {
            'Meta': {'object_name': 'SmkLqdSubsFreeFlavours', 'db_table': "u'smk_lqd_subs_free_flavours'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'create_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_create_user'", 'null': 'True', 'to': u"orm['backend.ResUsers']"}),
            'flavour1': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_flavour1'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_flavour2'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour3': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_flavour3'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            'flavour4': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_flavour4'", 'null': 'True', 'to': u"orm['backend.ProductProduct']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_update_user1'", 'null': 'True', 'to': u"orm['backend.ResUsers']"})
        },
        u'backend.smklqdsubsstandingorder': {
            'Meta': {'object_name': 'SmkLqdSubsStandingOrder', 'db_table': "u'smk_lqd_subs_standing_order'"},
            'battery_counter': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cancelled_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'cohort_index': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']"}),
            'flavours': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdSubsFlavours']", 'null': 'True', 'blank': 'True'}),
            'free_flavours': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdSubsFreeFlavours']", 'null': 'True', 'blank': 'True'}),
            'free_vaporizer_count': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_batchid': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'last_delivery': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'last_order_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'next_delivery': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'order_counter': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'potential_fraud': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'remote_addr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdRemoteAddr']", 'null': 'True', 'blank': 'True'}),
            'request_cancellation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'snail_mail_counter': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'successful_billing_count': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['backend']