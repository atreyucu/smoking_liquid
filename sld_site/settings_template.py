# Django settings for sld_site project.
import datetime

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ENABLED_SSL = True

import os

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'smoking_liquid',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'smokingadmin',
        'PASSWORD': 'sm0k1ngl1qu1d*-+/',
        'HOST': '127.0.0.1',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '5432',                      # Set to empty string for default.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/London'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = '/var/www/smokingliquiddirect.co.uk/web/'

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.

    os.path.join(PROJECT_ROOT,'media/fonts/'),
    os.path.join(PROJECT_ROOT,'media/css/'),
    os.path.join(PROJECT_ROOT,'media/js/'),
    os.path.join(PROJECT_ROOT,'media/images/'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'y#s^nkz)@=!f%h83*5z(s_)ax^rv-6pbna4k@%0g^@q#t^+&&v'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    #'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.cache.FetchFromCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'frontend.middlewares.SSLRedirect',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'sld_site.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'sld_site.wsgi.application'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",


)

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_ROOT, 'media/templates'),
)


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'cronjobs',
    'south',
    'captcha',
    'django_sagepay',
    'backend',
    'frontend',
    'common',
)

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(PROJECT_ROOT, 'media/cache'),
        'TIMEOUT': 60,
        'OPTIONS': {
            'MAX_ENTRIES': 1000
        }
    }
}


AUTHENTICATION_BACKENDS=('common.auth_backends.GeneralAuthBackend','django.contrib.auth.backends.ModelBackend')

#---------captcha settings----------------
CAPTCHA_FONT_SIZE = 28

CAPTCHA_LETTER_ROTATION = (-20,20)

CAPTCHA_NOISE_FUNCTIONS = ""


LOGIN_REDIRECT_URL = '/usersadmin/'

#Configuration of the invites
INV_FB_APP_ID = '552873124805448'

INV_FB_DIALOG_IMG = 'http://smokingliquiddirect.co.uk/static/social_invites/facebook.jpg'

FIRST_ORDER_PDF_PATH = os.path.join(PROJECT_ROOT, 'private/order.pdf')

COST_OF_CIGARRETES = 0.35

#EMAIL_HOST = 'localhost'
#EMAIL_PORT = 1025
#EMAIL_HOST_USER = 'noreply@lambertsmokingliquid.co.uk'
#EMAIL_HOST_PASSWORD = 'HUGFn34IDFyC'
#EMAIL_HOST_USER = 'Smoking_liquid@lambertsmokingliquid.co.uk'
#EMAIL_HOST_PASSWORD = 'IWyvqNbJD4'
SLD_FROM_USER = 'Smoking_liquid@lambertsmokingliquid.co.uk'
USE_SSL = False

#defaul value
EMAIL_USE_TLS = False

EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'lambecli@aol.com'
EMAIL_HOST_PASSWORD = 'SdA7EXhttO-INd67uXUntQ'


#standing information constants
#days counts
DEFAULT_REBILLING_PERIOD = 30
#days count
DEFAULT_DELIVERY_PERIOD = 60
#days count
DEFAULT_PRINTED_PERIOD = 46

#Payment module selection
DEFAULT_ACTIVE_PAYMENT_MODULE = 'PAYPAL'



#Sage Integration
SAGEPAY_URL = 'https://test.sagepay.com/gateway/service/vspserver-register.vsp'
SAGE_VENDOR_NAME = 'seller1seller'
SAGE_DEFAULT_TXTYPE = 'PAYMENT'
SAGE_VPS_PROTOCOL = '3.00'
SAGE_CURRENCY = 'GBP'


#Trasactions data
SLD_MONTH_ORDER_AMMOUNT = 19.99

#Payment repeat manager vars
PAYMENT_REPEAT_REBILLING_PERIOD = 30

PAYMENT_REPEAT_DELIVERY_PERIOD = 56

ALLOWED_HOSTS = ['*']

ACTIVATE_LOG_PAYMENT_NOTIFICATION = True

SITE_VENDOR_NAME = 'SMOKING LIQ'

DEFAULT_ORDER_NUMBER_PER_BATCH = 20

DEFAULT_LETTER_NUMBER_PER_BATCH = 20

REFUND_PERIOD = 33

COHORT_START_DATE = datetime.date(2015,1,1)

EMAIL_IMAGE = os.path.join(PROJECT_ROOT, 'media/images/email/header_email.jpg')

PUBLIC_SUBJECT_MESSAGE = 'SLD contact us message'