# -*- coding: latin-1 -*-
subject = 'Account Details'
content = """<p>Hi</p>

<p>We have created your account for you at smokingliquiddirect.co.uk</p>

<p>Please keep this password safe:</p>

<p>password:%s</p>

<p>Can we send you another free smoking pipe?</p>

<p>We've made it possible for you to send a message to your friends via Facebook, Twitter or email containing a link that's registered to you. When someone follows that link and signs up with us our system will identify it as your link that they followed and automatically credit you with a free smoking pipe (worth &pound;14.99).</p>

<p>The tracking is all automated so its really easy to do, just log into your account and start sharing.</p>

<p>Many Thanks</p>

<p>All the team<br>
lambertsmokingliquid.co.uk</p>
"""