import os
import sys

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(PROJECT_ROOT)
sys.path.append(os.path.abspath(os.path.join(PROJECT_ROOT,'../')))


os.environ["DJANGO_SETTINGS_MODULE"] = "sld_site.settings"

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()