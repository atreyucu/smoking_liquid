from backend.models import SmkLqdSubsStandingOrder
import sld_site.settings as settings
class RemoreAddrUtil(object):
    FORWARDED_FOR_FIELDS = [
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED_HOST',
        'HTTP_X_FORWARDED_SERVER',
    ]

    def process_request(self, request):
        for field in self.FORWARDED_FOR_FIELDS:
            if field in request.META:
                return field

        return request.META.get('REMOTE_ADDR')

class AnalyticFraud(object):
    def analice_fraud(self, standing_order):
        user_remote_addr = standing_order.remote_addr

        if user_remote_addr.smklqdsubsstandingorder_set.count() == 1:
            return False
        else:
            return True