def get_next_billing(date):
    if date.month == 1 and date.day > 28:
        return date.replace(date.year,date.month + 1,28)
    elif date.month == 1 and date.day <= 28:
        return date.replace(date.year,date.month + 1,date.day)
    elif date.month in [3,5,8,10] and date.day > 30:
        return date.replace(date.year,date.month + 1,30)
    elif date.month in [3,5,8,10] and date.day <= 30:
        return date.replace(date.year,date.month + 1,date.day)
    elif date.month == 12:
        return date.replace(date.year + 1,1,date.day)
    else:
        return date.replace(date.year,date.month + 1,date.day)



