import sld_site.settings as settings
from django.http import HttpResponseRedirect, HttpResponse

SSL = 'SSL'


def request_is_secure(request):
    if request.is_secure():
        return True

    # Handle forwarded SSL (used at Webfaction)
    if 'HTTP_X_FORWARDED_SSL' in request.META:
        return request.META['HTTP_X_FORWARDED_SSL'] == 'on'

    if 'HTTP_X_SSL_REQUEST' in request.META:
        return request.META['HTTP_X_SSL_REQUEST'] == '1'

    return False

class SSLRedirect:
    def process_request(self, request):
        if request_is_secure(request):
            request.IS_SECURE=True
        return None

    def process_view(self, request, view_func, view_args, view_kwargs):
        if SSL in view_kwargs:
            secure = view_kwargs[SSL]
            del view_kwargs[SSL]
        else:
            secure = False

        if settings.DEBUG or not settings.ENABLED_SSL:
            return None

        if getattr(settings, "TESTMODE", False):
            return None

        if not secure == request_is_secure(request):
            return self._redirect(request, secure)

    def _redirect(self, request, secure):
        if settings.DEBUG and request.method == 'POST':
            raise RuntimeError(
            """Django can't perform a SSL redirect while maintaining POST data.
                Please structure your views so that redirects only occur during GETs.""")

        protocol = secure and "https" or "http"

        host_name = request.META['HTTP_HOST']

        newurl = "%s://%s%s" % (protocol,host_name,request.get_full_path())

        return HttpResponseRedirect(newurl)



class EmailNotifier(object):
    def  process_response(self, request, response):
        try:
            if hasattr(request, 'session') and request.session.has_key('send_email'):
                from django.core.urlresolvers import resolve
                current_iew_name = resolve(request.path_info).url_name

                from aes.models import SiteAction
                from aes.tools import send_email

                actions = SiteAction.objects.filter(view_name=current_iew_name)

                if actions.count() > 0:
                    action = actions[0]

                    emails = action.automatedemail_set.filter(active=True)

                    for email in emails:
                        if request.session.has_key('to_email'):
                            send_email(email,request.session['to_email'],request)
                        else:
                            send_email(email,request.user.email,request)

                if request.session.has_key('send_email'):
                    request.session.__delitem__('send_email')

                if request.session.has_key('to_email'):
                    request.session.__delitem__('to_email')

        except Exception, ex:
            #return HttpResponse(ex)
            raise ex


        return response