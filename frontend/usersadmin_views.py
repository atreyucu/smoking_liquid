from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
import json
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
import smtplib
from django.core.mail import send_mail
from django.core.mail.message import EmailMessage
from backend.models import  ResPartner, ProductProduct, SmkLqdInvites, SmkLqdSubsStandingOrder, ResCountry, ResPartnerTitle, ResCountryState, SmkLqdMessage
import sld_site.settings as settings

@login_required()
def home(request):
    try:
        profile = request.user.profile
        customer = profile.customer
        standin_order = SmkLqdSubsStandingOrder.objects.get(customer = customer)
        cigarette_smoked_per_day = standin_order.cigarette_smoked_per_day
    except ObjectDoesNotExist, ex:
        raise ex

    fb_app_id = settings.INV_FB_APP_ID
    fb_app_dialog_img = settings.INV_FB_DIALOG_IMG
    invite = SmkLqdInvites()
    import datetime
    invite.create_date = datetime.datetime.now()
    invite.sender = request.user.profile.customer
    invite.was_followed = False
    invite.channel = 'fb'
    invite.save()
    host_name = request.META['HTTP_HOST']
    if request.is_secure():
        host_name = 'https://%s'%(host_name)
    else:
        host_name = 'http://%s'%(host_name)

    cigatteres_cost_saving = format(365*settings.COST_OF_CIGARRETES*cigarette_smoked_per_day,'.2f')

    absolute_url = host_name +  '/socialinvites/invite_proxy/' + str(invite.id)
    data = {'fb_app_id': fb_app_id, 'fb_invite_id':str(invite.id),'fb_app_dialog_img': fb_app_dialog_img, 'cigatteres_cost_saving': cigatteres_cost_saving, 'host_name': host_name, 'absolute_url': absolute_url}

    return render_to_response('useradmins/useradmins_home.html', {'fb_data': data},context_instance=RequestContext(request))

@login_required()
def your_subscription(request):
    user = request.user
    try:
        profile = user.profile
        customer = profile.customer
    except ObjectDoesNotExist:
        return HttpResponse('Error, Please contact with your administrator')

    all_flavours = ProductProduct.objects.filter(is_flavour = True)

    flavours_return = []
    for f in all_flavours:
        flavours_temp = {}
        flavours_temp['product_id'] = f.id
        try:
            strength = f.product_strength
            if strength != None:
                flavours_temp['strength'] = strength.display_name
                flavours_temp['item_name'] = f.name_template + strength.display_name
            else:
                flavours_temp['strength'] = ""
                flavours_temp['item_name'] = f.name_template
        except ObjectDoesNotExist:
            flavours_temp['strength'] = ""
            flavours_temp['item_name'] = f.name_template
        flavours_return.append(flavours_temp)

    standing_order_ = customer.smklqdsubsstandingorder_set.all()

    if len(standing_order_) > 0:
        standing_order = standing_order_[0]
        try:
            flavours_user = standing_order.flavours
        except ObjectDoesNotExist:
            flavours_user = None
    else:
        standing_order = None
        flavours_user = None

    if flavours_user != None:
        flavours_user = {'flavour_1': flavours_user.flavour1_id, 'flavour_2': flavours_user.flavour2_id,
                        'flavour_3': flavours_user.flavour3_id, 'flavour_4': flavours_user.flavour4_id,
                        'flavour_5': flavours_user.flavour5_id, 'flavour_6': flavours_user.flavour6_id,
                        'flavour_7': flavours_user.flavour7_id, 'flavour_8': flavours_user.flavour8_id,
                        'flavour_9': flavours_user.flavour9_id,'flavour_10': flavours_user.flavour10_id}
    else:
        flavours_user = {'flavour_1': 0, 'flavour_2': 0,
                        'flavour_3': 0, 'flavour_4': 0,
                        'flavour_5': 0,'flavour_6': 0,
                        'flavour_7': 0,'flavour_8': 0,
                        'flavour_9': 0,'flavour_10': 0,}

    allow_free_flavours = True

    four_free_flavours_user = {'flavour_1':1, 'flavour_2': 1,
                               'flavour_3':1, 'flavour_4': 1}

    if standing_order is not None:
        fourfreeflavours = standing_order.free_flavours
        if fourfreeflavours is not None:
            four_free_flavours_user = {'flavour_1':fourfreeflavours.flavour1_id, 'flavour_2': fourfreeflavours.flavour2_id,
                                       'flavour_3':fourfreeflavours.flavour3_id, 'flavour_4': fourfreeflavours.flavour4_id}

    next_order_date = None
    next_billing_date = None
    if standing_order is not None:
        order_date = standing_order.next_printed
        next_billing_date = standing_order.next_billing
        next_order_date = None
        if order_date is not None:
            next_order_date = order_date

   
    put_js_function = True
    activate = False
    if standing_order is not None and standing_order.status == 'n':
        cancel_link_text = '(click to activate)'
        activate = True
    else:
        cancel_link_text = '(click to cancel)'
        activate = False

    return render_to_response('useradmins/useradmins_subscriptions.html',
                              {'activate':activate,'cancel_link_text':cancel_link_text,
                               'put_js_function': put_js_function, 'vendor_name': settings.SITE_VENDOR_NAME,
                               'earned_pipes': str(standing_order.free_vaporizer_count),
                               'next_order': next_order_date,'next_billing': next_billing_date,
                               'flavours_user': flavours_user,
                               'flavours': flavours_return, 'allow_free_flavours':allow_free_flavours,
                               'four_free_flavours_user':four_free_flavours_user},context_instance=RequestContext(request))
    
   
@login_required()
def change_status_order(request):
    user = request.user
    general_user = user.generaluser
    standing_order_information = general_user.standing_order_information
    data = {'success': True, 'text': ''}
    if standing_order_information.cancelled:
        data['text'] = '(click to cancel)'
    else:
        standing_order_information.cancelled = True
        standing_order_information.unsubscribe_recived = datetime.now()
        data['text'] = '(cancellation pending)'
        to_value = [standing_order_information.generaluser.email]
        subject_value = 'Order Cancellation Request'
        message_value = """
        Hi

        Were sorry to hear that your leaving us.

        We've actioned your request and your subscription will be cancelled shortly.

        many thanks

        All the team
        lambertsmokingliquid.co.uk"""
        if settings.USE_SSL:
            try:
                smt = smtplib.SMTP_SSL(host=settings.EMAIL_HOST,port=settings.EMAIL_PORT)
                message = EmailMessage(subject_value,message_value,from_email=settings.EMAIL_HOST_USER,to=to_value)
                smt.sendmail(settings.EMAIL_HOST_USER,to_value,message.message().as_string())
                smt.close()
            except Exception:
                pass
        else:
            try:
                send_mail(subject_value, message_value, settings.SLD_FROM_USER,to_value)
            except Exception:
                pass
    standing_order_information.save()
    return HttpResponse(json.dumps(data), mimetype='application/json')


@login_required()
def your_details(request):
    countries = ResCountry.objects.all()
    countries_data = []
    for c in countries:
        countries_temp = {}
        countries_temp['codeid'] = c.id
        countries_temp['code_name'] = c.name
        countries_data.append(countries_temp)

    titles = ResPartnerTitle.objects.filter(domain='smoking')
    titles_data = []
    for t in titles:
        titles_data_temp = {}
        titles_data_temp['titleid'] = t.id
        titles_data_temp['title'] = t.shortcut
        titles_data.append(titles_data_temp)


    profile = request.user.profile
    customer = profile.customer
    ResPartner().respartneraddress_set.filter(type='delivery')

    private_address = customer.respartneraddress_set.filter(type='default')
    private_address_data = {}
    if len(private_address) > 0:
        private_address = private_address[0]
        private_address_data['email'] = customer.username
         #your_address = ResPartnerAddress()
        private_address_data['your_title'] = private_address.title_id
        private_address_data['your_firstname'] = private_address.name
        private_address_data['your_secondname'] = private_address.second_name
        private_address_data['your_hbn'] = private_address.hbn
        private_address_data['your_streetaddress'] = private_address.street
        private_address_data['your_secondlineaddress'] = private_address.street2
        private_address_data['your_towncity'] = private_address.city
        private_address_data['your_county'] = private_address.state_id
        private_address_data['your_country'] = private_address.country_id
        private_address_data['your_postcode'] = private_address.zip

        all_your_county = ResCountryState.objects.filter(country_id = private_address.country_id)
        all_your_county_data = []
        for yc in all_your_county:
            all_your_county_temp = {}
            all_your_county_temp['codeid'] = yc.id
            all_your_county_temp['code_name'] = yc.name
            all_your_county_data.append(all_your_county_temp)

    delivery_address = customer.respartneraddress_set.filter(type='delivery')
    delivery_address_data = {}
    if len(delivery_address) > 0:
        delivery_address = delivery_address[0]
        delivery_address_data['delivery_title'] = delivery_address.title_id
        delivery_address_data['delivery_firstname'] = delivery_address.name
        delivery_address_data['delivery_secondname'] = delivery_address.second_name
        delivery_address_data['delivery_hbn'] = delivery_address.hbn
        delivery_address_data['delivery_streetaddress'] = delivery_address.street
        delivery_address_data['delivery_secondlineaddress'] = delivery_address.street2
        delivery_address_data['delivery_towncity'] = delivery_address.city
        delivery_address_data['delivery_county'] = delivery_address.state_id
        delivery_address_data['delivery_country'] = delivery_address.country_id
        delivery_address_data['delivery_postcode'] = delivery_address.zip


        all_delivery_county = ResCountryState.objects.filter(country_id = delivery_address.country_id)
        all_delivery_county_data = []
        for yc in all_delivery_county:
            all_delivery_county_temp = {}
            all_delivery_county_temp['codeid'] = yc.id
            all_delivery_county_temp['code_name'] = yc.name
            all_delivery_county_data.append(all_delivery_county_temp)

    billing_address = customer.respartneraddress_set.filter(type = 'billing')
    billing_address_data = {}
    if len(billing_address) > 0:
        billing_address = billing_address[0]
        billing_address_data['billing_title'] = billing_address.title_id
        billing_address_data['billing_firstname'] = billing_address.name
        billing_address_data['billing_secondname'] = billing_address.second_name
        billing_address_data['billing_hbn'] = billing_address.hbn
        billing_address_data['billing_streetaddress'] = billing_address.street
        billing_address_data['billing_secondlineaddress'] = billing_address.street2
        billing_address_data['billing_towncity'] = billing_address.city
        billing_address_data['billing_county'] = billing_address.state_id
        billing_address_data['billing_country'] = billing_address.country_id
        billing_address_data['billing_postcode'] = billing_address.zip


        all_billing_county = ResCountryState.objects.filter(country_id = billing_address.country_id)
        all_billing_county_data = []
        for yc in all_billing_county:
            all_billing_county_temp = {}
            all_billing_county_temp['codeid'] = yc.id
            all_billing_county_temp['code_name'] = yc.name
            all_billing_county_data.append(all_billing_county_temp)

    return render_to_response('useradmins/useradmins_your_details.html',
                              {'countries': countries_data,
                               'titles': titles_data,
                               'private_address': private_address_data,
                               'delivery_address': delivery_address_data,
                               'billing_address': billing_address_data,

                               'your_counties': all_your_county_data,
                               'delivery_counties':all_delivery_county_data,
                               'billing_counties':all_billing_county_data,
                              }, context_instance=RequestContext(request))


def get_counties(request):
    if request.is_ajax():
        country = request.GET['country']
        counties = ResCountryState.objects.filter(country_id = country)
        dict_counties = {}
        for county in counties:
            dict_counties[county.id] = county.name

        return HttpResponse(json.dumps(dict_counties), mimetype='application/json')
    else:
        return HttpResponse()

@login_required()
def save_your_details(request):
    try:
        show_message = False
        if request.method == 'POST':
            django_user = request.user
            profile = django_user.profile
            customer = profile.customer

            if request.POST['password'] and request.POST['password'] != '' and request.POST['password'] == request.POST['re-password']:
                django_user.set_password(request.POST['password'])
                customer.password_blank = request.POST['password']

            django_user.username = request.POST['email']
            django_user.first_name = request.POST['your_firstname']
            django_user.second_name = request.POST['your_secondname']
            django_user.email = request.POST['email']
            django_user.save()

            customer.name = '%s %s'%(request.POST['your_firstname'],request.POST['your_secondname'])
            customer.username = request.POST['email']
            customer.save()

            soi = customer.smklqdsubsstandingorder_set.all()[0]

            soi.customer_email = request.POST['email']
            soi.customer_name = '%s %s'%(request.POST['your_firstname'],request.POST['your_secondname'])
            soi.customer_sip_code = request.POST['your_postcode']
            soi.save()


            private_address = customer.respartneraddress_set.filter(type = 'default')
            if len(private_address) > 0:
                private_address = private_address[0]
                private_address.title_id = request.POST['your_title']
                private_address.name = request.POST['your_firstname']
                private_address.second_name = request.POST['your_secondname']
                private_address.hbn = request.POST['your_hbn']
                private_address.street = request.POST['your_streetaddress']
                private_address.street2 = request.POST['your_secondlineaddress']
                private_address.city = request.POST['your_towncity']
                private_address.state_id = request.POST['your_county']
                private_address.country_id = request.POST['your_country']
                private_address.zip = request.POST['your_postcode']
                private_address.save()

            delivery_address = customer.respartneraddress_set.filter(type = 'delivery')
            if len(delivery_address) > 0:
                delivery_address = delivery_address[0]
                delivery_address.title_id = request.POST['delivery_title']
                delivery_address.name = request.POST['delivery_firstname']
                delivery_address.second_name = request.POST['delivery_secondname']
                delivery_address.hbn = request.POST['delivery_hbn']
                delivery_address.street = request.POST['delivery_streetaddress']
                delivery_address.street2 = request.POST['delivery_secondlineaddress']
                delivery_address.city = request.POST['delivery_towncity']
                delivery_address.state_id = request.POST['delivery_county']
                delivery_address.country_id = request.POST['delivery_country']
                delivery_address.zip = request.POST['delivery_postcode']
                delivery_address.save()

            billing_address = customer.respartneraddress_set.filter(type = 'billing')
            if len(billing_address) > 0:
                billing_address = billing_address[0]
                billing_address.title_id = request.POST['billing_title']
                billing_address.name = request.POST['billing_firstname']
                billing_address.second_name = request.POST['billing_secondname']
                billing_address.hbn = request.POST['billing_hbn']
                billing_address.street = request.POST['billing_streetaddress']
                billing_address.street2 = request.POST['billing_secondlineaddress']
                billing_address.city = request.POST['billing_towncity']
                billing_address.state_id = request.POST['billing_county']
                billing_address.country_id = request.POST['billing_country']
                billing_address.zip = request.POST['billing_postcode']
                billing_address.save()
            show_message = True




        countries = ResCountry.objects.all()
        countries_data = []
        for c in countries:
            countries_temp = {}
            countries_temp['codeid'] = c.id
            countries_temp['code_name'] = c.name
            countries_data.append(countries_temp)

        titles = ResPartnerTitle.objects.filter(domain='smoking')
        titles_data = []
        for t in titles:
            titles_data_temp = {}
            titles_data_temp['titleid'] = t.id
            titles_data_temp['title'] = t.shortcut
            titles_data.append(titles_data_temp)


        profile = request.user.profile
        customer = profile.customer
        ResPartner().respartneraddress_set.filter(type='delivery')

        private_address = customer.respartneraddress_set.filter(type='default')
        private_address_data = {}
        if len(private_address) > 0:
            private_address = private_address[0]
            private_address_data['email'] = customer.username
             #your_address = ResPartnerAddress()
            private_address_data['your_title'] = private_address.title_id
            private_address_data['your_firstname'] = private_address.name
            private_address_data['your_secondname'] = private_address.second_name
            private_address_data['your_hbn'] = private_address.hbn
            private_address_data['your_streetaddress'] = private_address.street
            private_address_data['your_secondlineaddress'] = private_address.street2
            private_address_data['your_towncity'] = private_address.city
            private_address_data['your_county'] = private_address.state_id
            private_address_data['your_country'] = private_address.country_id
            private_address_data['your_postcode'] = private_address.zip

            all_your_county = ResCountryState.objects.filter(country_id = private_address.country_id)
            all_your_county_data = []
            for yc in all_your_county:
                all_your_county_temp = {}
                all_your_county_temp['codeid'] = yc.id
                all_your_county_temp['code_name'] = yc.name
                all_your_county_data.append(all_your_county_temp)

        delivery_address = customer.respartneraddress_set.filter(type='delivery')
        delivery_address_data = {}
        if len(delivery_address) > 0:
            delivery_address = delivery_address[0]
            delivery_address_data['delivery_title'] = delivery_address.title_id
            delivery_address_data['delivery_firstname'] = delivery_address.name
            delivery_address_data['delivery_secondname'] = delivery_address.second_name
            delivery_address_data['delivery_hbn'] = delivery_address.hbn
            delivery_address_data['delivery_streetaddress'] = delivery_address.street
            delivery_address_data['delivery_secondlineaddress'] = delivery_address.street2
            delivery_address_data['delivery_towncity'] = delivery_address.city
            delivery_address_data['delivery_county'] = delivery_address.state_id
            delivery_address_data['delivery_country'] = delivery_address.country_id
            delivery_address_data['delivery_postcode'] = delivery_address.zip


            all_delivery_county = ResCountryState.objects.filter(country_id = delivery_address.country_id)
            all_delivery_county_data = []
            for yc in all_delivery_county:
                all_delivery_county_temp = {}
                all_delivery_county_temp['codeid'] = yc.id
                all_delivery_county_temp['code_name'] = yc.name
                all_delivery_county_data.append(all_delivery_county_temp)

        billing_address = customer.respartneraddress_set.filter(type = 'billing')
        billing_address_data = {}
        if len(billing_address) > 0:
            billing_address = billing_address[0]
            billing_address_data['billing_title'] = billing_address.title_id
            billing_address_data['billing_firstname'] = billing_address.name
            billing_address_data['billing_secondname'] = billing_address.second_name
            billing_address_data['billing_hbn'] = billing_address.hbn
            billing_address_data['billing_streetaddress'] = billing_address.street
            billing_address_data['billing_secondlineaddress'] = billing_address.street2
            billing_address_data['billing_towncity'] = billing_address.city
            billing_address_data['billing_county'] = billing_address.state_id
            billing_address_data['billing_country'] = billing_address.country_id
            billing_address_data['billing_postcode'] = billing_address.zip


            all_billing_county = ResCountryState.objects.filter(country_id = billing_address.country_id)
            all_billing_county_data = []
            for yc in all_billing_county:
                all_billing_county_temp = {}
                all_billing_county_temp['codeid'] = yc.id
                all_billing_county_temp['code_name'] = yc.name
                all_billing_county_data.append(all_billing_county_temp)

        return render_to_response('useradmins/useradmins_your_details.html',
                                  {'countries': countries_data,
                                   'titles': titles_data,
                                   'private_address': private_address_data,
                                   'delivery_address': delivery_address_data,
                                   'billing_address': billing_address_data,

                                   'your_counties': all_your_county_data,
                                   'delivery_counties':all_delivery_county_data,
                                   'billing_counties':all_billing_county_data,
                                   'show_message': show_message
                                  }, context_instance=RequestContext(request))

    except Exception, ex:
        #return HttpResponse(ex)
        raise ex

@login_required()
def contact_us(request):
    show_delivery_message = request.session.has_key('show_delivery_message_alert') and request.session['show_delivery_message_alert']

    if request.session.has_key('show_delivery_message_alert'):
        request.session.__delitem__('show_delivery_message_alert')

    return render_to_response('useradmins/useradmins_contact_us.html', {'show_delivery_message': show_delivery_message}, context_instance=RequestContext(request))


@login_required()
def save_conact_us(request):
    subject = request.POST['subject']
    body = request.POST['body']

    try:
        profile = request.user.profile
        customer = profile.customer
    except ObjectDoesNotExist:
        return HttpResponse('Error, please contact with your administrator')

    message = SmkLqdMessage()
    message.body = body
    message.subject = subject
    message.sender = customer
    message.sender_email = customer.username
    message.sender_name = customer.name
    message.from_site = 'sld'
    message.sent_on = datetime.now()
    message.save()
    request.session['show_delivery_message_alert'] = True
    from common.emails import send_email

    send_email('new_message')
    return redirect('/usersadmin/contactus/')



@login_required()
def change_flavour(request):
    if request.is_ajax:
        if request.GET.has_key('flavour') and request.GET.has_key('number'):
            flavour = request.GET.get('flavour')
            number = request.GET.get('number')
            try:
                profile = request.user.profile
                customer = profile.customer
                standin_order = SmkLqdSubsStandingOrder.objects.get(customer = customer)
                flavour_user = standin_order.flavours
                flavour_user.change_flavour(product_id=flavour, number=number)
            except ObjectDoesNotExist:
                data = {'success':False}
                json_data = json.dumps(data)
                return HttpResponse(json_data, content_type='application/json')
            data = {'success': True}
        else:
            data = {'success':False}
        json_data = json.dumps(data)
        return HttpResponse(json_data, content_type='application/json')
    else:
        return HttpResponse('Only for ajax request.')


@login_required()
def free_change_flavour(request):
    if request.is_ajax:
        if request.GET.has_key('flavour') and request.GET.has_key('number'):
            flavour = request.GET.get('flavour')
            number = request.GET.get('number')
            try:
                profile = request.user.profile
                customer = profile.customer
                standin_order = SmkLqdSubsStandingOrder.objects.get(customer = customer)
                free_flavour_user = standin_order.free_flavours
                free_flavour_user.change_flavour(product_id=flavour, number=number)
            except ObjectDoesNotExist:
                data = {'success':False}
                json_data = json.dumps(data)
                return HttpResponse(json_data, content_type='application/json')

            data = {'success': True}
        else:
            data = {'success':False}
        json_data = json.dumps(data)
        return HttpResponse(json_data, content_type='application/json')
    else:
        return HttpResponse('Only for ajax request.')


@login_required()
def activate_account(request):
    try:
        profile = request.user.profile
        customer = profile.customer
        standin_order = SmkLqdSubsStandingOrder.objects.get(customer = customer)
    except ObjectDoesNotExist:
        return HttpResponse('Error, please contact with your administrator')

    request.session['paypal_activate_account'] = standin_order.id
    import uuid

    trans_id = str(uuid.uuid4()).replace('-','')
    request.session['paypal_custom_trans_id'] = trans_id

    if(settings.DEFAULT_ACTIVE_PAYMENT_MODULE == 'PAYPAL'):
        return render_to_response('frontend/paypal_autosend_reactivate_form.html',{'custom_trans_id' : trans_id}, context_instance=RequestContext(request))
    else:
        standin_order.cancelled = False
        standin_order.save()
        customer.save()
        return HttpResponseRedirect('/')
