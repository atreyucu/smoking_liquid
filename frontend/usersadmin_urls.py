from django.conf.urls import patterns

urlpatterns = patterns('',
   (r'^$', 'frontend.usersadmin_views.home',{'SSL':True}),
   (r'^yourdetails/$', 'frontend.usersadmin_views.your_details',{'SSL':True}),
   (r'^yoursubscription/$', 'frontend.usersadmin_views.your_subscription',{'SSL':True}),
   (r'^save_your_details/$', 'frontend.usersadmin_views.save_your_details',{'SSL':True}),
   (r'^contactus/$', 'frontend.usersadmin_views.contact_us',{'SSL':True}),
   (r'^save_conact_us/$', 'frontend.usersadmin_views.save_conact_us',{'SSL':True}),
   #(r'^get_towns/$', 'frontend.usersadmin_views.get_towns',{'SSL':True}),
   (r'^get_counties/$', 'frontend.usersadmin_views.get_counties',{'SSL':True}),
   (r'^change_flavour/$', 'frontend.usersadmin_views.change_flavour',{'SSL':True}),
   (r'^free_change_flavour/$', 'frontend.usersadmin_views.free_change_flavour',{'SSL':True}),
   (r'^change_status_order/$', 'frontend.usersadmin_views.change_status_order',{'SSL':True}),
   (r'^activate_account/$', 'frontend.usersadmin_views.activate_account',{'SSL':True}),



)
