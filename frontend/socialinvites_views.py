# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
import simplejson
from django.shortcuts import render_to_response
import sld_site.settings as settings
from django.http import HttpResponseRedirect, HttpResponse
from backend.models import SmkLqdInvites, SmkLqdSubsStandingOrder
import datetime
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.contrib.auth.decorators import permission_required, login_required
import smtplib
from django.core.mail.message import EmailMessage, EmailMultiAlternatives
from django.template.context import RequestContext

@login_required()
def facebook_popup_json(request):
    if(request.is_ajax()):
        try:
            profile = request.user.profile
            customer = profile.customer
            standin_order = SmkLqdSubsStandingOrder.objects.get(customer = customer)
            cigarette_smoked_per_day = standin_order.cigarette_smoked_per_day
        except ObjectDoesNotExist:
            return HttpResponse('Error, please contact with your administrator')

        fb_app_id = settings.INV_FB_APP_ID
        fb_app_dialog_img = settings.INV_FB_DIALOG_IMG
        invite = SmkLqdInvites()
        invite.sent_on = datetime.datetime.now()
        invite.sender = customer
        invite.followed_on = datetime.datetime.now()
        invite.was_followed = False
        invite.channel = 'fb'
        invite.save()
        host_name = request.META['HTTP_HOST']
        if request.is_secure():
            host_name = 'https://%s'%(host_name)
        else:
            host_name = 'http://%s'%(host_name)
        db_user = User.objects.get(id=request.user.id)

        cigatteres_cost_saving = format(365*settings.COST_OF_CIGARRETES*cigarette_smoked_per_day,'.2f')

        json_data = simplejson.dumps({'fb_app_id': fb_app_id, 'fb_invite_id':str(invite.id),'fb_app_dialog_img': fb_app_dialog_img, 'cigatteres_cost_saving': cigatteres_cost_saving, 'host_name': host_name})

        return HttpResponse(json_data, content_type="application/json")
    else:
        return HttpResponse()

@login_required()
#@permission_required('general_auth.can_access_useradmins')
def email_popup(request):
    if(request.is_ajax()):
        try:
            profile = request.user.profile
            customer = profile.customer
            standin_order = SmkLqdSubsStandingOrder.objects.get(customer = customer)
            cigarette_smoked_per_day = standin_order.cigarette_smoked_per_day
        except ObjectDoesNotExist:
            return HttpResponse('Error, please contact with your administrator')
        invite = SmkLqdInvites()
        invite.sent_on = datetime.datetime.now()
        invite.sender = customer
        invite.followed_on = datetime.datetime.now()
        invite.was_followed = False
        invite.channel = 'em'
        invite.save()
        subject_value = 'From ' + request.user.first_name + ' ' + request.user.last_name

        cigatteres_cost_saving = format(365*settings.COST_OF_CIGARRETES*cigarette_smoked_per_day,'.2f')
        host_name = request.META['HTTP_HOST']
        if request.is_secure():
            host_name = 'https://%s'%(host_name) + '/socialinvites/invite_proxy/' + str(invite.id)
        else:
            host_name = 'http://%s'%(host_name) + '/socialinvites/invite_proxy/' + str(invite.id)
        json = simplejson.dumps({'cigatteres_cost_saving': cigatteres_cost_saving, 'link':host_name, 'subject': subject_value })
        return HttpResponse(json, content_type="application/json")
        #return render_to_response('social_invites/sl_email_popup.html',{'cigatteres_cost_saving': cigatteres_cost_saving, 'link':host_name, 'subject': subject_value },context_instance= RequestContext(request))
    if(request.method == 'POST'):
            to_values = request.POST['to'].split(',')
            subject_value = 'From ' + request.user.first_name + ' ' + request.user.last_name
            message_value = request.POST['message']
            message_value = message_value.replace('Hi','<p>Hi</p>')
            message_value = message_value.replace('...','</p><p>...')
            message_value = message_value + '</p>'
            message_value = message_value.replace('Just','<p>Just')
            try:
                for to_value in to_values:
                    if settings.USE_SSL:
                        smt = smtplib.SMTP_SSL(host=settings.EMAIL_HOST,port=settings.EMAIL_PORT)
                        message = EmailMessage(subject_value,message_value,from_email='smoking_liquid@lambertsmokingliquid.co.uk',to=[to_value])
                        smt.sendmail(settings.EMAIL_HOST_USER,to_value,message.message().as_string())
                        smt.close()
                    else:
                        html = message_value
                        msg = EmailMultiAlternatives(subject_value, '', from_email='smoking_liquid@lambertsmokingliquid.co.uk', to=[to_value])
                        msg.attach_alternative(html, "text/html")
                        msg.send()
            except Exception,ex:
                return HttpResponse(ex)
                return HttpResponseRedirect('/usersadmin/')
            return HttpResponseRedirect('/usersadmin/')
    else:
        return HttpResponse()

@login_required()
#@permission_required('general_auth.can_access_useradmins')
def twitter_popup(request):
    if(request.is_ajax()):
        try:
            profile = request.user.profile
            customer = profile.customer
            standin_order = SmkLqdSubsStandingOrder.objects.get(customer = customer)
            cigarette_smoked_per_day = standin_order.cigarette_smoked_per_day
        except ObjectDoesNotExist:
            return HttpResponse('Error, please contact with your administrator')

        cigatteres_cost_saving = format(365*settings.COST_OF_CIGARRETES*cigarette_smoked_per_day,'.2f')
        invite = SmkLqdInvites()
        invite.sent_on = datetime.datetime.now()
        invite.sender = customer
        invite.followed_on = datetime.datetime.now()
        invite.was_followed = False
        invite.channel = 'tw'
        invite.save()
        host_name = request.META['HTTP_HOST']
        if request.is_secure():
            host_name = 'https://%s'%(host_name)
        else:
            host_name = 'http://%s'%(host_name)
        result_dict = {'saving_cigarretes': cigatteres_cost_saving, 'host_name': host_name, 'inv_id': invite.id}
        json = simplejson.dumps(result_dict)
        return HttpResponse(json, content_type="application/json")
    else:
        return HttpResponse('Only ajax request')


def invite_proxy(request,invite_id):
    try:
        invite = SmkLqdInvites.objects.get(id=invite_id)
        invite.was_followed = True
        invite.followed_on = datetime.datetime.now()
        invite.save()

        request.session['sld_customer_follow_invite'] = invite_id

        return HttpResponseRedirect('/')
    except ObjectDoesNotExist:
        return HttpResponseRedirect('/')